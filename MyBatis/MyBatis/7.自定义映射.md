# 自定义映射

[TOC]

## 处理字段和属性的映射关系

### 第一种

**id设置主键**

**result设置普通**

```xml
<resultMap id="userMap" type="User">
	<id property="id" column="id"></id>
	<result property="userName" column="user_name"></result>
	<result property="password" column="password"></result>
	<result property="age" column="age"></result>
	<result property="sex" column="sex"></result>
</resultMap>
```

### 第二种

可以通过为字段起别名的方式

```xml
select id,user_name userName ,password,age,sex from t_user 
```

### 第三种

设置全局配置，将自动映射为驼峰

```xml
<setting name="mapUnderscoreToCamelcase"value="true"/>
```

## 多对一映射处理

### 级联方式处理映射关系



```xml
<resultMap id="empDeptMap" type="Emp">
	<id column="eid" property="eid"></id>
	<result column="ename" property="ename"></result>
	<result column="age" property="age"></result>
	<result column="sex" property="sex"></result>
    <!-- 设置映射 -->
	<result column="did" property="dept.did"></result>
	<result column="dname" property="dept.dname"></result>
</resultMap>
<!--Emp getEmpAndDeptByEid(@Param("eid") int eid);-->
<select id="getEmpAndDeptByEid" resultMap="empDeptMap">
select emp.*,dept.* from t_emp emp left join t_dept dept on emp.did =dept.did where emp.eid = #{eid}
</select>
```

### 使用association处理映射关系

```xml
<association property="dept" javaType="Dept">
	<id column="did" property="did"></id>
	<result column="dname" property="dname"></result>
</association>
```

### 分步查询

查询员工信息

```xml
<resultMap id="empDeptStepMap" type="Emp">
	<id column="eid" property="eid"></id>
	<result column="ename" property="ename"></result>
	<result column="age" property="age"></result>
	<result column="sex" property="sex"></result>
<!--
select：设置分步查询，查询某个属性的值的sql的标识（namespace.sqlId）
column：将sql以及查询结果中的某个字段设置为分步查询的条件
-->
<association property="dept"
	select="com.atguigu.MyBatis.mapper.DeptMapper.getEmpDeptByStep" column="did">
</association>
</resultMap>

<!--Emp getEmpByStep(@Param("eid") int eid);-->
<select id="getEmpByStep" resultMap="empDeptStepMap">
	select * from t_emp where eid = #{eid}
</select>
```

根据员工所对应的部门id查询部门信息

```xml
<!--Dept getEmpDeptByStep(@Param("did") int did);-->
<select id="getEmpDeptByStep" resultType="Dept">
	select * from t_dept where did = #{did}
</select>
```

## 一对多映射处理

### collection

```xml
<resultMap id="deptEmpMap" type="Dept">
<id property="did" column="did"></id>
<result property="dname" column="dname"></result>
<!--ofType：设置collection标签所处理的集合属性中存储数据的类型-->
<collection property="emps" ofType="Emp">
	<id property="eid" column="eid"></id>
	<result property="ename" column="ename"></result>
	<result property="age" column="age"></result>
	<result property="sex" column="sex"></result>
	</collection>
</resultMap>
<!--Dept getDeptEmpByDid(@Param("did") int did);-->
<select id="getDeptEmpByDid" resultMap="deptEmpMap">
select dept.*,emp.* from t_dept dept left join t_emp emp on dept.did =emp.did where dept.did = #{did}
</select>
```

## 分步查询

查询部门信息

```xml
<resultMap id="deptEmpStep" type="Dept">
	<id property="did" column="did"></id>
	<result property="dname" column="dname"></result>
	<collection property="emps" fetchType="eager"
		select="com.atguigu.MyBatis.mapper.EmpMapper.getEmpListByDid" column="did">
	</collection>
</resultMap>
<!--Dept getDeptByStep(@Param("did") int did);-->
<select id="getDeptByStep" resultMap="deptEmpStep">
select * from t_dept where did = #{did}
</select>
```

根据部门id查询部门中的所有员工

```xml
<!--List<Emp> getEmpListByDid(@Param("did") int did);-->
<select id="getEmpListByDid" resultType="Emp">
select * from t_emp where did = #{did}
</select>
```

