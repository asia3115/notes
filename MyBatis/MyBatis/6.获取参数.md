# 获取参数

[TOC]

## 单参数

```xml
<!-- 使用#{}接受参数 -->
<select id="">
    select * from 表名 where id =#{id}
</select>
```

## 多个参数

```xml
<!-- 使用${arg}或者#{param},接受参数 -->
<!-- 按照顺序写索引 -->
<select id="">
    select * from 表名 where id =#{arg0},name=#{param2}
</select>
```

## Map集合

```xml
<!-- 使用#{键的名字}接受参数 -->
<select id="">
    select * from 表名 where id =#{username},name=#{password}
</select>
```

## POJO

```XML
 <!-- 使用#{属性}接受参数 -->
<insert id="">
     insert into dish_flavor values (#{id},#{username},#{password})
 </insert>
```

## @Param

```java
User checkLoginByParam (@Param("username") String username,@Param("password") String password)
```

```xml
<select id="">
    select * from 表名 where id =#{username},name=#{password}
</select>
```

