# 分页

[TOC]

## 快速开发

1.添加配置类

```java
@Configuration
@MapperScan("com.atguigu.mybatisplus.mapper") //可以将主类中的注解移到此处
public class MybatisPlusConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    interceptor.addInnerInterceptor(new
    PaginationInnerInterceptor(DbType.MYSQL));
    return interceptor;
	}
}
```

2.测试

```java
@Test
public void testPage(){
    //设置分页参数
    Page<User> page = new Page<>(1, 5);
    userMapper.selectPage(page, null);
    //获取分页数据
    List<User> list = page.getRecords();
    list.forEach(System.out::println);
    System.out.println("当前页："+page.getCurrent());
    System.out.println("每页显示的条数："+page.getSize());
    System.out.println("总记录数："+page.getTotal());
    System.out.println("总页数："+page.getPages());
    System.out.println("是否有上一页："+page.hasPrevious());
    System.out.println("是否有下一页："+page.hasNext());
}
```

## xml自定义分页

UserMapper中定义接口方法

```java
//page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位
Page<User> selectPageVo(@Param("page") Page<User> page, @Param("age") Integer age);
```

UserMapper.xml中编写SQL

```properties
<!--IPage<User> selectPageVo(Page<User> page, Integer age);-->
	<select id="selectPageVo" resultType="User">
	SELECT <include refid="BaseColumns"></include> FROM t_user WHERE age > #{age}
</select>
```

