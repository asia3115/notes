# 多线程

[TOC]

## 解释

让程序同时做多件事情

并发：在同一时刻，有多个指令在单个CPU上交替执行
并行：在同一时刻，有多个指令在多个CPU上同时执行

## 实现方式

### 继承Thread类

```java
//继承Thread类
public class MyThread extends Thread {
    //重写run方法
    @Override
    public void run() {
        // 线程执行的逻辑
        System.out.println("线程正在执行");
    }

    public static void main(String[] args) {
        // 创建线程对象
        MyThread thread = new MyThread();

        // 启动线程
        thread.start();
    }
}

```

1. 自己定义一个类继承Thread
2. 重写run方法
3. 创建子类的对象，并启动线程

### 实现Runnable接口

```java
public class MyRunnable implements Runnable {
    @Override
    public void run() {
        // 线程执行的逻辑
        System.out.println("线程正在执行");
    }

    public static void main(String[] args) {
        // 创建实现了 Runnable 接口的对象
        MyRunnable runnable = new MyRunnable();

        // 创建线程对象，将实现了 Runnable 接口的对象作为参数传入
        Thread thread = new Thread(runnable);

        // 启动线程
        thread.start();
    }
}

```

1. 自己定义一个类实现Runnable接口
2. 重写里面的run方法
3. 创建自己的类的对象
4. 创建一个Thread类的对象，并开启线程

### Callable接口和Future接口

```java
public class MyCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        // 执行耗时的任务，返回结果
        Thread.sleep(2000);
        return "任务执行完成";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 创建实现了 Callable 接口的对象
        MyCallable callable = new MyCallable();

        // 创建 FutureTask 对象，将 Callable 对象作为参数传入
        FutureTask<String> futureTask = new FutureTask<>(callable);

        // 创建线程对象，将 FutureTask 对象作为参数传入
        Thread thread = new Thread(futureTask);

        // 启动线程
        thread.start();

        // 获取任务执行结果
        String result = futureTask.get();
        System.out.println("任务结果：" + result);
    }
}

```

1. 创建一个类MyCallable实现Callable接口
2. 重写call(是有返回值的，表示多线程运行的结果)
3. 创建MyCallable的对象（表示多线程要执行的任务)
4. 创建FutureTask的对象（作用管理多线程运行的结果）
5. 创建Thread类的对象，并启动（表示线程）

## 方法

| 方法                             | 说明                                     |
| -------------------------------- | ---------------------------------------- |
| String getName（）               | 返回此线程的名称                         |
| void setName(String name         | 设置线程的名字（构造方法也可以设置名字） |
| static Thread currentThread（）  | 获取当前线程的对象                       |
| static void sleep(long time)     | 让线程休眠指定的时间，单位为毫秒         |
| setPriority(int newPriority)     | 设置线程的优先级                         |
| final int getpriority（）        | 获取线程的优先级                         |
| final void setDaemon(boolean on) | 设置为守护线程[^1]                       |
| public static void yield（）     | 出让线程/礼让线程[^2]                    |
| public static void join（）      | 插入线程/插队线程[^3]                    |

## 同步代码块(锁)

### 同步代码块

```JAVA
synchronized(锁){
操作共享数据的代码
}
```

1. 锁默认打井，有一个线程进去了，锁自动关闭
2. 里面的代码全部执行完毕，线程出来，锁自动打开
3. 锁必须唯一

### 同步方法

```JAVA
修饰符 synchronized 返回值类型 方法名（方法参数）{}
```

1. 同步方法是锁住方法里面所有的代码
2. 锁对象不能自己指

### LOCK锁

```java
public class LockExample {
    private static int count = 0;
    //创建LOCK锁
    private static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(new IncrementTask());
        Thread t2 = new Thread(new IncrementTask());

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Final count: " + count);
    }

    static class IncrementTask implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                lock.lock(); // 获取锁
                try {
                    count++; // 对共享资源进行操作
                } finally {
                    lock.unlock(); // 释放锁，确保在任何情况下都能正确释放资源
                }
            }
        }
    }
}

```



| 方法     | 说明   |
| -------- | ------ |
| lock()   | 获得锁 |
| unlock() | 释放   |

[^1]:在后台运行的线程，它不会阻止程序的终止。当所有非守护线程结束时，程序就会终止，不管守护线程是否完成。
[^2]:线程的礼让，就是让出自己抢到的执行权
[^3]:在一个线程执行期间，将另一个线程插入到该线程的执行过程中