# 工具类和static静态

[TOC]

## 工具类

帮助我们做一些事情的，但是不描述任何事物的类

```java
public class Arrutil{
    private Arrutil（）{}
    public static int getMax(…){…}
	public static int getMin(…){…J
	public static int getsum(..){...}
	public static int getAvg(…){…}
}
```

## static静态

### 解释

static表示静态，是Java中的一个修饰符，可以修饰成员方法，成员变量

### 静态变量

1. 被该类所有对象共享.

2. 属于类。

3. 随着类的加载而加载，优先于对象存在

4. 调用方法

   - 类名调用(推荐)

   - 对象名调用

### 静态方法

1. 多用在测试类和工具类中

2. Javabean类中很少会用

3. 调用方法

   - 类名调用(推荐)

   - 对象名调用

### 注意点

1. 静态方法中，只能访问静态。
2. 非静态方法可以访问所有。
3. 静态方法中没有this关键字