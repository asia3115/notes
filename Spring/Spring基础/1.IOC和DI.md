# IOC/DI
.
[TOC]

## IOC

![image](https://biji-typora.oss-cn-shanghai.aliyuncs.com/6af37271-f589-4f7f-8b44-64d4e6fc9dc3-19940075.jpg)

![image](https://biji-typora.oss-cn-shanghai.aliyuncs.com/db989354-219b-4770-ae67-27d812e70eea-19940075.jpg)

![image](https://biji-typora.oss-cn-shanghai.aliyuncs.com/4b2c21f3-9b55-4313-93cc-a48e6e4036ed-19940075.jpg)

```java
<bean id="bookService"class="com.itheima.service.impl.BookServiceImpl"></bean>
```

​	**id代表起的名字,class代表类的位置**

![image](https://biji-typora.oss-cn-shanghai.aliyuncs.com/cb4abb0b-4138-480d-81cc-4279a16b07dd-19940075.jpg)

## DI	

![image-20230606150522407](https://biji-typora.oss-cn-shanghai.aliyuncs.com/image-20230606150522407.png)

![image-20230606150650423](https://biji-typora.oss-cn-shanghai.aliyuncs.com/image-20230606150650423.png)

![image-20230606150655186](https://biji-typora.oss-cn-shanghai.aliyuncs.com/image-20230606150655186.png)
